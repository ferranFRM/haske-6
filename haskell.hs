-------------------------------------------------------------------------------------
----------------------------- PRÀCTICA DE HASKELL 2019 ------------------------------
-------------------------------------------------------------------------------------
-------------------------- Ferran Rodríguez, Miguel Ángel ---------------------------

import Data.Char
import Data.List

------------------------------ Definició de CONSTANTS --------------------------------
--------------------------------------------------------------------------------------

taulerInicial = Tauler [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]

reiEscapa = Tauler [(DB,Posicio('e',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),(TB,Posicio('c',4)),(RB,Posicio('e',2))]
reiNoEscapa = Tauler [(DB,Posicio('e',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),(AB,Posicio('e',5)),(TB,Posicio('c',4)),(RB,Posicio('d',4))]
reiEsSalvat = Tauler [(DB,Posicio('e',8)),(AN,Posicio('c',7)),(PN,Posicio('d',7)),(AN,Posicio('f',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(TB,Posicio('e',6)),(AB,Posicio('e',5)),(PN,Posicio('h',5)),(RB,Posicio('d',4))]
reiEsSalvat2 = Tauler [(DB,Posicio('e',8)),(AN,Posicio('c',7)),(PN,Posicio('d',7)),(AB,Posicio('a',6)),(RN,Posicio('c',6)),(AN,Posicio('e',6)),(AB,Posicio('e',5)),(PN,Posicio('h',5)),(RB,Posicio('d',4))]


type Partida = (Tauler, Color)
type Jugada = (Peca, Posicio, Posicio, String)

data Posicio = Posicio (Char, Int) deriving (Eq)
data Peca = PN | PB | CN | CB | AN | AB | TN | TB | DN | DB | RN | RB | BE deriving (Eq)
data Tauler = Tauler [(Peca, Posicio)]
data Color = Blanc | Negre deriving (Show,Eq)

----------------------------------- INSTANCIACIONS -----------------------------------
--------------------------------------------------------------------------------------

instance Show Peca where
 show PN = "p"
 show PB = "P"
 show CN = "c"
 show CB = "C"
 show AN = "a"
 show AB = "A"
 show TN = "t"
 show TB = "T"
 show DN = "d"
 show DB = "D"
 show RN = "r"
 show RB = "R"
 show BE = "."  --Buit error
 
instance Show Posicio where
 show (Posicio (a,b)) = "(" ++ show a++ "," ++ show b ++ ")"
 
instance Show Tauler where
 show (Tauler t) = "   ==========\n" ++ mostrarPeces (Tauler t) ++ "   ==========\n" ++ "    abcdefgh"

instance Ord Posicio where
 compare (Posicio(a,b)) (Posicio(c,d))
  | a == c && b == d = EQ
  | a <= c && b >= d = LT
  | otherwise = GT

----------------------------------MOSTRAR PECES---------------------------------------
--------------------------------------------------------------------------------------
successorPosicio :: Posicio -> Posicio
successorPosicio (Posicio (a,b))
 | a=='h' && b==1 = error("Successor posicio erroni")
 | a=='h' && b>1 = Posicio('a',(b-1))
 | a<'h' = Posicio((a!+1),b)

posicioLimit :: Posicio
posicioLimit = Posicio('h',1)

mostrarPecaTauler :: (Peca, Posicio) -> String
mostrarPecaTauler (peca, Posicio(a,b))
 | a == 'a' = show b ++ "- |" ++ show peca    --pos inicial
 | a == 'h' = show peca ++ "|\n"              --pos final
 | otherwise = show peca                      --qualsevol


iMostrarPeces :: Posicio -> Tauler -> String
iMostrarPeces pos (Tauler []) = if (pos /= posicioLimit) then mostrarPecaTauler (BE,pos) ++ iMostrarPeces (successorPosicio pos) (Tauler[]) else mostrarPecaTauler (BE,pos)
iMostrarPeces pos (Tauler t) 
 | pos == posicioLimit && pos == (snd x) = mostrarPecaTauler x        --si es la ultima i son la mateixa
 | pos == posicioLimit = mostrarPecaTauler (BE,pos)                   --es la ultima
 | pos == (snd x) = mostrarPecaTauler x ++ iMostrarPeces (successorPosicio pos) (Tauler(tail t))
 | otherwise = mostrarPecaTauler (BE,pos) ++ iMostrarPeces (successorPosicio pos) (Tauler(t))
 where 
  x = head t

mostrarPeces :: Tauler -> String
mostrarPeces (Tauler t) = iMostrarPeces (Posicio ('a',8)) (Tauler t)


--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------


--------------------------------------------------
--------------------MOVIMENTS---------------------
--------------------------------------------------

{- Retorna tots els moviments del rei possible a través de la Posicio entrada.
   Un Rei pot fer un salt en qualsevol direcció, sense sortir-se del tauler -}
   -- EX: movimentRei (Posicio ('a',2))
movimentRei :: Posicio -> [Posicio]
movimentRei (Posicio (g,h)) = (vt g h 1) ++ (vt g h (-1)) ++ (vt (g!+1) h 0) ++ (vt (g!+(-1)) h 0) ++ (vt (g!+1) h 1) ++ (vt (g!+1) h (-1)) ++ (vt (g!+(-1)) h (-1)) ++ (vt (g!+(-1)) h 1)
  where 
      vt g h x = if (dinsDelsMarges g (h+x)) then [Posicio (g,h+x)] else []

{- Retorna tots els moviments del Cavall possible a través de la Posicio entrada.
   Un Cavall fa salts en forma de L i pot tenir fins a un total de 8 possibilitats -}
   -- EX: movimentCavall (Posicio ('a',2))
movimentCavall :: Posicio -> [Posicio]
movimentCavall (Posicio (g,h)) = (vld g (h+1) (-2)) ++ (vld g (h+1) 2) ++ (vld g (h-1) (-2)) ++ (vld g (h-1) 2) ++ (vld g (h+2)(-1)) ++ (vld g (h+2) 1) ++ (vld g (h-2) (-1)) ++ (vld g (h-2) 1)
  where
     vld g h a = if (dinsDelsMarges (g!+a) h) then [Posicio (g!+a,h)] else []

{- Retorna tots els moviments de l'alfil possible a través de la Posicio entrada.
   Un Alfil fa salts en diagonal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
   -- EX: movimentAlfil (Posicio ('c',3))
movimentAlfil :: Posicio -> [Posicio]
movimentAlfil (Posicio (g,h))= (diagEsAd g (h+1) (-1)) ++ (diagEsAb g (h-1) (-1)) ++ (diagDrAd g (h+1) 1) ++ (diagDrAb g (h-1) 1)

{- Retorna tots els moviments de la torre possible a través de la Posicio entrada.
   Una torre fa salts en horitzontal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
   -- EX: movimentTorre (Posicio ('d',2))
movimentTorre :: Posicio -> [Posicio]
movimentTorre (Posicio (g,h)) = (hrz g h 1) ++ (hrz g h (-1)) ++ (vrt g h 1) ++ (vrt g h (-1))

{- Retorna tots els moviments del peo possible a través de la Posicio entrada.
   Un Peó pot fer 1 o 2 salts en la primera jugada i només 1 salt en la resta.
   Com que no es sap qui és el jugador que està efectuant el moviment es considerara ...  -}
   -- EX: movimentPeo (Posicio ('d',2)) Negre, movimentPeo (Posicio ('d',2)) Blanc
movimentPeo :: Posicio -> Color -> [Posicio]
movimentPeo (Posicio (g,h)) c = if (c==Blanc) then (if (h==2) then ((vt g h 1) ++ (vt g h 2)) else (vt g h 1)) else (if (h==7) then ((vt g h (-1)) ++ (vt g h (-2))) else (vt g h (-1)))
  where 
      vt g h x = if (dinsDelsMarges g (h+x)) then [Posicio (g,h+x)] else []



--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

--------------------------------------------------
--------------------AUXILIARS---------------------
--------------------------------------------------
{- Retorna el color de la Peca. Precondició: La peca ha d'existir i no pot ser B -}
colorPeca :: Peca -> Color
colorPeca x = if (x==PB || x==CB || x==AB || x==TB || x==DB || x==RB) then Blanc else Negre

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAd :: Char -> Int -> Int -> [Posicio]
diagEsAd g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagEsAd (g!+a) (h+1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAb :: Char -> Int -> Int -> [Posicio]
diagEsAb g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagEsAb (g!+a) (h-1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAd :: Char -> Int -> Int -> [Posicio]
diagDrAd g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagDrAd (g!+a) (h+1) 1)) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAb :: Char -> Int -> Int -> [Posicio]
diagDrAb g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagDrAb (g!+a) (h-1) 1)) else []

{- Retorna tots els moviments en horitzontal a partir de la posició donada, la direcció depén del valor de la columna 'a' -}
hrz :: Char -> Int -> Int -> [Posicio]
hrz g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (hrz (g!+a) h a)) else []

{- Retorna tots els moviments en vertical a partir de la posició donada, la direcció depén del valor de la fila 'a' -}
vrt :: Char -> Int -> Int -> [Posicio]
vrt g h a = if (dinsDelsMarges g (h+a)) then ([Posicio (g,h+a)] ++ (vrt g (h+a) a)) else []


{- La funció dinsDelsMarges retorna cert si la posició es dins dels marges del Tauler, és a dir, files de 1 a 8 i columnes de 'a' a 'h', altrament fals-}
dinsDelsMarges :: Char -> Int -> Bool
dinsDelsMarges x y
  | y<1 || y>8 = False
  | x>'h' || x<'a' = False
  | otherwise = True
  
{- !+ és un operador que retorna la lletra que ve despres de n salts (Int) de la lletra que hem introduit (Char), si no pertany a cap columna retorna 'z' -}
(!+) :: Char -> Int -> Char
ch !+ a = if ((vv a)<'i' && (vv a)>'`') then vv a else 'z'
  where
     vv a = (chr(ord ch + a))


--- ALGU ENTRE
{- La funció alguEntre retorna cert si hi ha algú entre les posicions donades, sense contemplar la primera. -}
-- EX: alguEntre taulerInicial (Posicio ('a',7)) (Posicio ('a',3)), alguEntre taulerInicial (Posicio ('a',2)) (Posicio ('f',7))
alguEntre :: Tauler -> Posicio -> Posicio -> Bool
alguEntre t (Posicio (a,b)) (Posicio (x,y))
  | a == x && b == y = False
  | (obtPeca t (Posicio (x,y)))/=BE && (colorPeca (obtPeca t (Posicio (a,b))))==(colorPeca (obtPeca t (Posicio (x,y)))) = True     -- Darrera posició hi ha un aliat
  | ((obtPeca t (Posicio (a,b)))==CN) || ((obtPeca t (Posicio (a,b)))==CB) = False
  | a == x && b < y = alguPosicions t (init (tail (vertical (Posicio (a,b)) (Posicio (x,y)) 1)))
  | a == x && b > y = alguPosicions t (init (tail (vertical (Posicio (a,b)) (Posicio (x,y)) (-1))))
  | a > x && b == y = alguPosicions t (init (tail (horitzontal (Posicio (a,b)) (Posicio (x,y)) (-1))))
  | a < x && b == y = alguPosicions t (init (tail (horitzontal (Posicio (a,b)) (Posicio (x,y)) 1)))
  | a < x && b < y = alguPosicions t (init (tail (diagonalBA (Posicio (a,b)) (Posicio (x,y)) 1)))           -- ('a',1) a ('b',2)
  | a < x && b > y = alguPosicions t (init (tail (diagonalAB (Posicio (a,b)) (Posicio (x,y)) 1)))           -- ('a',8) a ('h',1)
  | a > x && b < y = alguPosicions t (init (tail (diagonalBA (Posicio (a,b)) (Posicio (x,y)) (-1))))        -- ('h',1) a ('a',8)
  | a > x && b > y = alguPosicions t (init (tail (diagonalAB (Posicio (a,b)) (Posicio (x,y)) (-1))))           -- ('h',8) a ('a',1)
  | otherwise = error "Moviment no correcte"
  
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en horitzontal
   Precondicions: Les dues posicions es troben a la mateixa fila i la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
   -- EX: horitzontal (Posicio ('a',8)) (Posicio ('g',8)) (1)
horitzontal :: Posicio -> Posicio -> Int -> [Posicio]
horitzontal (Posicio (a,b)) (Posicio (x,y)) f
 | (a,b) == (x,y) = [(Posicio (x,y))]
 | otherwise = [(Posicio (a,b))] ++ (horitzontal (Posicio (((!+) a f),b)) (Posicio (x,y)) f)
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en vertical
   Precondicions: Les dues posicions es troben a la mateixa columna, la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
   -- EX: vertical (Posicio ('a',7)) (Posicio ('a',5)) (-1)
vertical :: Posicio -> Posicio -> Int -> [Posicio]
vertical (Posicio (a,b)) (Posicio (x,y)) f
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (vertical (Posicio (a,b+f)) (Posicio (x,y)) f)
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per adalt a l'esquerra i acaba abaix a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la dreta en el tauler, i han d'estar dins dels marges del tauler -}
   -- EX: diagonalAB (Posicio ('h',8)) (Posicio ('e',5)) (-1)
diagonalAB :: Posicio -> Posicio -> Int -> [Posicio]
diagonalAB (Posicio (a,b)) (Posicio (x,y)) f
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (diagonalAB (Posicio (((!+)a f),b-1)) (Posicio (x,y)) f)
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per abaix a l'esquerra i acaba adalt a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la l'esquerra en el tauler, i han d'estar dins dels marges del tauler -}
   -- EX: diagonalBA (Posicio ('a',1)) (Posicio ('d',4)) (1), diagonalBA (Posicio ('h',1)) (Posicio ('a',8)) (-1)
diagonalBA :: Posicio -> Posicio -> Int -> [Posicio]
diagonalBA (Posicio (a,b)) (Posicio (x,y)) f
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (diagonalBA (Posicio (((!+)a f),b+1)) (Posicio (x,y)) f)
 
{- Retorna cert si algunes de les posicions donades té alguna Peça diferent de B, és a dir, si no és buida, altrament fals
   Precondició: Les posicions entrades tenen que ser legals (dins del tauler) -}
alguPosicions :: Tauler -> [Posicio] -> Bool
alguPosicions (Tauler t) [] = False
alguPosicions (Tauler t) [y] = if ((posicioBuida (Tauler t) y) == False) then True else False
alguPosicions (Tauler t) (y:ys) = if ((posicioBuida (Tauler t) y) == False) then True else (alguPosicions (Tauler t) ys)

{- Retorna Cert si la Posicio entrada no es troba al Tauler entrat, és a dir, si no hi ha cap peça en aquella posició, altrament retorna fals. -}
posicioBuida :: Tauler -> Posicio -> Bool
posicioBuida (Tauler []) _ = False
posicioBuida (Tauler [x]) p = if ((snd x) == p) then False else True
posicioBuida (Tauler (x:xs)) p = if ((snd x) == p) then False else (posicioBuida (Tauler xs) p)


{- La funció moviment ... saber donada una Peca i una posició, quines serien totes les posicions on podria anar en un tauler buit. -}
moviment :: Peca -> Posicio -> [Posicio]
moviment x y
  | (x == PN) || (x == PB) = movimentPeo y (colorPeca x)
  | (x == CN) || (x == CB) = movimentCavall y
  | (x == AN) || (x == AB) = movimentAlfil y
  | (x == TN) || (x == TB) = movimentTorre y
  | (x == DN) || (x == DB) = (movimentTorre y)++(movimentAlfil y)
  | (x == RN) || (x == RB) = movimentRei y
  | otherwise = error "No existeix aquesta Peca"

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

-------------------AUXILIARS----------------------
-------------------FesJugada----------------------
--------------------------------------------------
menorPosicio :: Posicio -> Posicio -> Bool
menorPosicio (Posicio(a,b)) (Posicio(c,d))
 | b > d = True
 | b == d && a < c = True
 | otherwise = False
 

inserirOrdenat :: (Peca, Posicio) -> [(Peca, Posicio)] -> [(Peca, Posicio)]
inserirOrdenat item [] = [item]
inserirOrdenat item (first:ls)
 | (snd item) == (snd first) = item : ls            --Son el mateix
 | menorPosicio (snd item) (snd first) = item : first : ls
 | otherwise = first : (inserirOrdenat item ls)

eliminarPosicio :: Posicio -> [(Peca, Posicio)] -> [(Peca, Posicio)]
eliminarPosicio pos [] = [] -- no s'ha trobat
eliminarPosicio pos (first:ls)
 | pos == (snd first) = ls                   --Son el mateix
 | otherwise = first : (eliminarPosicio pos ls) 

{- 
pre: La jugada es legal
La funció fesJugada retorna el Tauler inicial però amb la jugada efectuada. 
Y es origen, Z es desti
-}
fesJugada :: Tauler -> Jugada -> Tauler
fesJugada (Tauler t) (x,y,z,_) = Tauler(inserirOrdenat (x,z) (eliminarPosicio y t))

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

  



--------------------------------------- INICI FUNCIÓ ESCAC --------------------------------------
-------------------------------------------------------------------------------------------------

{- La funció escac retorna cert si al tauler entrat el rei del color entrat té alguna peça rival que li pot fer escac, altrament fals -}
escac :: Tauler -> Color -> Bool
escac (Tauler t) c = taulerEscac (Tauler t) (Tauler t) c (posAct (Tauler t) c 'R')

{- Retorna cert si alguna peca contraria al color donat pot matar al rei del contrari, que es troba a la posicio donada, altrament fals -}
taulerEscac :: Tauler -> Tauler -> Color -> Posicio -> Bool
taulerEscac (Tauler t) (Tauler [x]) c p = if (esRival (fst x) c) then (if ((faEscac x p) && (jugadaLegal (Tauler [x]) ((obtPeca (Tauler [x]) (snd x)),(snd x),p,""))) then True else False) else False
taulerEscac (Tauler t) (Tauler (x:xs)) c p = if (esRival (fst x) c) then (if ((faEscac x p) && (jugadaLegal (Tauler t) ((obtPeca (Tauler [x]) (snd x)),(snd x),p,""))) then True else taulerEscac (Tauler t) (Tauler xs) c p) else taulerEscac (Tauler t) (Tauler xs) c p

{- Retorna la posicio de la peça introduida com a caracter al tauler entrat, amb el color entrat -}
   -- EX: posAct taulerInicial Negre 'R'
posAct :: Tauler -> Color -> Char -> Posicio
posAct (Tauler t) c o = if ((posPecTauler (Tauler t) (pecaResultant c o) 0)==32) then (error "No existeix al tauler") else snd (t!!(posPecTauler (Tauler t) (pecaResultant c o) 0))

{- Retorna a quina posició de l'array de Tauler es troba la Peca entrada -}
posPecTauler :: Tauler -> Peca -> Int -> Int
posPecTauler (Tauler [x]) p i = if (p == fst x) then i else 32
posPecTauler (Tauler (x:xs)) p i = if (p == fst x) then i else (posPecTauler (Tauler xs) p (i+1))

{- Retorna la Peca en funció del color entrar i el caracter -}
pecaResultant :: Color -> Char -> Peca
pecaResultant Blanc x = if (x=='P') then PB else (if (x=='C') then CB else (if (x=='A') then AB else (if (x=='T') then TB else (if (x=='D') then DB else RB))))
pecaResultant Negre x = if (x=='P') then PN else (if (x=='C') then CN else (if (x=='A') then AN else (if (x=='T') then TN else (if (x=='D') then DN else RN))))

{- Retorna cert si la peca entrada no pertany al color entrat, és a dir, si són rivals -}
esRival :: Peca -> Color -> Bool
esRival p c = if (((p==PN || p==CN || p==AN || p==TN || p==DN || p==RN) && (c == Blanc)) || ((p==PB || p==CB || p==AB || p==TB|| p==DB|| p==RB) && (c == Negre))) then True else False

{- Retorna cert si la peca en la posició entrada té algun moviment que la porti a la segona posicio, altrament retorna fals -}
   -- EX: faEscac (TN, (Posicio ('a',7))) (Posicio ('a',1)), faEscac (AN, (Posicio ('c',8))) (Posicio ('h',3))
faEscac :: (Peca,Posicio) -> Posicio -> Bool
faEscac (x,y) p = if ((find (==p) (moviment x y)) == Nothing) then False else True

---------------------------------------- FI FUNCIÓ ESCAC ----------------------------------------
-------------------------------------------------------------------------------------------------
{- La funció jugadaLegal: Primer mirem si la jugada indicada la pot fer la peça indicada, si no pot retornem directament fals.
Altrament, si pot, mirem si es cavall o no...
Si no és cavall mirem si hi ha algú pel camí marcat entre les dues posicions, si hi ha algú retornem Fals, altrament mirem si la posició final
indicada és buida o hi ha una peça rival, si compleix una de les dues condicions retorna Cert, és a dir, la jugada és valida, altrament fals. -}
-- COMPROVAR QUE EN LA POSICION ESTE DICHA PIEZA



--------------------------------------- INICI FUNCIÓ ESCACIMAT ----------------------------------
-------------------------------------------------------------------------------------------------

escacMat :: Tauler -> Color -> Bool
escacMat (Tauler t) c = if (escac (Tauler t) c) then (if (esPotProtegir (Tauler t) ((pecaResultant c 'R'),(posAct (Tauler t) c 'R'))) then False else True) else False

{- Retorna una llista posicions no ocupades possibles per a la peca i posicio on es troba-}
movimentsPossibles :: Tauler -> (Peca, Posicio) -> [Posicio]
movimentsPossibles (Tauler []) _ = []
movimentsPossibles (Tauler t) (peca,pos) = [x | x <- (moviment peca pos), (posicioBuida (Tauler t) x) && (not (alguEntre (Tauler t) pos x)) ]

iPotEscapar :: Tauler -> (Peca, Posicio) -> [Posicio] -> Bool 
iPotEscapar _ _ [] = False                            --esta buit
iPotEscapar (Tauler t) (peca, posIni) (pos:xs) =
 if esEscac then (iPotEscapar (Tauler t) (peca, posIni) xs) else True
 where
  esEscac = (escac (fesJugada (Tauler t) (peca,posIni,pos,"")) (colorPeca peca))

{- Retorna cert si el rei es pot moure a una de les seves possibles caselles sense que li poguin fer escac -}
potEscapar :: Tauler -> (Peca, Posicio) -> Bool
potEscapar (Tauler t) (peca,pos) = iPotEscapar (Tauler t) (peca,pos) (movimentsPossibles (Tauler t) (peca,pos))

pecesQueFanEscac :: Tauler -> Color -> Posicio -> [(Peca, Posicio)]
pecesQueFanEscac (Tauler []) _ _ = []
pecesQueFanEscac (Tauler [x]) c p = if ((esRival (fst x) c) && (faEscac x p)) then [x] else []
pecesQueFanEscac (Tauler (x:xs)) c p = if ((esRival (fst x) c) && (faEscac x p)) then (x : pecesQueFanEscac (Tauler xs) c p) else (pecesQueFanEscac (Tauler xs) c p)

{- Amb tauler original t, tauler llista restant items, color a buscar, retorna la llista de possibles posicions dels aliats del color de la forma: Peca, PosInici, [PosicionsDesti]-}
posicionsPossiblesTotsAliats :: Tauler -> Tauler -> Color -> [((Peca,Posicio),[Posicio])]
posicionsPossiblesTotsAliats (Tauler[]) _ _ = []
posicionsPossiblesTotsAliats _ (Tauler[]) _ = []
posicionsPossiblesTotsAliats (Tauler t) (Tauler (item:items)) c = if (not(esRival (fst item) c)) then ( ((fst item), (snd item)), [x | x <- (movimentsPossibles (Tauler t) item)]) : (posicionsPossiblesTotsAliats (Tauler t) (Tauler items) c) else (posicionsPossiblesTotsAliats (Tauler t) (Tauler items) c)

{- Retorna cert si amb totes les posicions es fan escac, retorna false si hi ha alguna amb la que no es faci escac, i per tant es pogui defensar-}
-- totesPosicioPecaFaEscac reiNoEscapa RN (Posicio('c',6)) [(Posicio ('c',7)),(Posicio ('c',5)),(Posicio ('d',6)),(Posicio ('b',6)),(Posicio ('d',7)),(Posicio ('d',5)),(Posicio ('b',5)),(Posicio ('b',7))]
-- totesPosicioPecaFaEscac reiEsSalvat AN (Posicio ('c',7)) [(Posicio ('b',8)),(Posicio ('b',6)),(Posicio ('a',5)),(Posicio ('d',8)),(Posicio ('d',6)),(Posicio ('e',5))]
totesPosicioPecaFaEscac :: Tauler -> Peca -> Posicio -> [Posicio] -> Bool
totesPosicioPecaFaEscac (Tauler []) _ _ _ =  True
totesPosicioPecaFaEscac _ _ _ [] = True --ha arribat al final i totes han fet escac
totesPosicioPecaFaEscac (Tauler t) peca posIni (item:items) = if not esEscac then False else totesPosicioPecaFaEscac (Tauler t) peca posIni items  --si hi ha algu que no pogui fer escac, vol dir que s'ha pogut defensar, else continuar buscant
 where
   esEscac = (escac (fesJugada (Tauler t) (peca,posIni,item,"")) (colorPeca peca))
   
grrr :: Tauler -> (Peca, Posicio, Posicio) -> Bool
grrr (Tauler t) (p,p2,p3) = escac (fesJugada (Tauler t) (p,p2,p3,"")) Negre


--protegir: obteni tots els enemics que fan escac, per cada moviment possible de les peces aliades, si hi ha algu entre la enemiga i el rei
iEsPotProtegir :: Tauler -> (Peca, Posicio) -> [((Peca,Posicio),[Posicio])] -> Bool
iEsPotProtegir (Tauler []) _ _ = False
iEsPotProtegir _ _ [] = False
iEsPotProtegir _ _ [((_),[])] = False
iEsPotProtegir (Tauler t) rei (mov:movimentsTots) = if (not totesFanEscac) then True else (iEsPotProtegir (Tauler t) rei movimentsTots)
 where
  --enemics = pecesQueFanEscac (Tauler t) (colorPeca peca) pos
  totesFanEscac = totesPosicioPecaFaEscac (Tauler t) (fst (fst mov)) (snd (fst mov)) (snd mov) 

-- {- Retorna cert si hi ha alguna peca capaç de protegir al rei; posar-se devant del rei que esta a punt de ser atacat -}
-- EX: esPotProtegir reiNoEscapa (RN,Posicio('c',6)), esPotProtegir reiEscapa (RN,Posicio('c',6))
esPotProtegir :: Tauler -> (Peca, Posicio) -> Bool
esPotProtegir (Tauler t) (peca, pos) = iEsPotProtegir (Tauler t) (peca,pos) aliats
 where 
  aliats = posicionsPossiblesTotsAliats (Tauler t) (Tauler t) (colorPeca peca)


-- esPodenMatar :: Tauler -> Peca -> Bool

-- escacMat :: Tauler -> Peca -> Bool
-- escacMat (Tauler t) peca = (potEscapar ) || (esPotProtegir ) || (esPodenMatar 





------------------------------------ INICI FUNCIÓ JugadaLegal -----------------------------------
-------------------------------------------------------------------------------------------------

{- Retorna cert si la peca que es troba a la posicio inicial por arribar a la posicio final amb les propietats d'aquesta.
   El cavall és l'unica peça que pot saltar, per això es tracta diferent a la resta.
   Si la peça de la jugada no coincideix amb la que hi ha a la posició inicial també retorna fals   -}
   -- EX: jugadaLegal taulerInicial (CN, (Posicio ('b',8)), (Posicio ('a',6)))
jugadaLegal :: Tauler -> Jugada -> Bool
jugadaLegal (Tauler t) (p, p1, p2,_) = if (faEscac (p,p1) p2) then (if ((obtPeca (Tauler t) p1)==CB || (obtPeca (Tauler t) p1)==CN) then (posFinal (Tauler t) p1 p2) else (if (alguEntre (Tauler t) p1 p2) then False else (posFinal (Tauler t) p1 p2))) else False
  where 
    posFinal (Tauler t) p1 p2 = if ((posPosTauler (Tauler t) p2 0) == 32) then True else (if (esRival (obtPeca (Tauler t) p2) (colorPeca (obtPeca (Tauler t) p1))) then True else False)


-- Retorna la peca que hi ha a la posicio donada al tauler donat
-- Precondicio? Posicio valida?
obtPeca :: Tauler -> Posicio -> Peca
obtPeca (Tauler t) p = if ((posPosTauler (Tauler t) p 0)>31) then BE else (fst (t!!(posPosTauler (Tauler t) p 0)))

{- Retorna a quina posició de l'array de Tauler es troba la posicio entrada -}
posPosTauler :: Tauler -> Posicio -> Int -> Int
posPosTauler (Tauler [x]) p i = if (p == snd x) then i else 32
posPosTauler (Tauler (x:xs)) p i = if (p == snd x) then i else (posPosTauler (Tauler xs) p (i+1))

-------------------------------------- FI FUNCIÓ JugadaLegal ------------------------------------
-------------------------------------------------------------------------------------------------


----------------------------MAIN------------------------------
-- errorException :: IOError -> IO a
-- errorException e
--  | isDoesNotExistError e = print("ERROR: No existeix el fitxer")
--  | errorDesconegut e = error("ERROR: desconegut")

main = do 
   putStrLn "Benvingut a l'analitzador d'Escacs"
   putStrLn "Introdueix el nom del fitxer:"
   nomFitxer <- getLine
   putStrLn ("Obrint el fitxer: " ++ nomFitxer)
   tractarFitxer nomFitxer
--   fitxer <- try $ readFile nomFitxer 
--   putStrLn fitxer
--  -- analitzarPartida lines fitxer

tractarFitxer nomFitxer = do
   f <- readFile nomFitxer
   let filesJugades = lines f
   tractarFiles filesJugades

tractarFiles linies = do
   if null linies
   then putStrLn "Fi"
   else do
      print (head linies)
      tractarFiles (tail linies)